# Перепишіть домашнє завдання попереднього уроку (сервіс для скорочення посилань) таким чином,
# щоб у нього була основна частина, яка відповідала би за логіку роботи та надавала узагальнений інтерфейс,
# і модуль представлення, який відповідав би за взаємодію з користувачем. При заміні останнього на інший,
# який взаємодіє з користувачем в інший спосіб, програма має продовжувати коректно працювати.

import shelve


def create_short_url(db, short_name, initial_url):
    # A shortened URL saving in the database
    if short_name not in db:
        db[short_name] = initial_url
        print("The URL has been created:", short_name)
    else:
        print("This short name already exists. Choose another one.")


def get_initial_url(db, short_name):
    # Get an initial URL from the database.
    if short_name in db:
        initial_url = db[short_name]
        print(f"The origin URL is: {initial_url}")
    else:
        print("There is no such short name.")


def main():
    with shelve.open("link_base") as db:
        while True:
            print("1. Get an initial URL")
            print("2. Create a short URL")
            print("3. Exit")
            option = input("Enter your choice: ")

            if option == "1":
                short_name = input("Enter a short name: ")
                get_initial_url(db, short_name)

            elif option == "2":
                short_name = input("Enter a short name: ")
                initial_url = input("Enter the origin URL: ")
                create_short_url(db, short_name, initial_url)

            elif option == "3":
                print("See you next time!")
                break

            else:
                print("Invalid option")


if __name__ == "__main__":
    main()

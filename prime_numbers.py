# Створіть модуль для отримання простих чисел. Імпортуйте його з іншого модуля. Імпортуйте його окремі імена.

"""Прості числа - це натуральні числа більші за 1, які мають лише два дільники: 1 і саме число."""


def check_prime_num(n):
    if n <= 1:
        return False
    if n <= 3:
        return True
    elif n % 2 == 0 or n % 3 == 0:
        return False
    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


def get_prime_num(limit):
    primes = []
    for num in range(2, limit + 1):
        if check_prime_num(num):
            primes.append(num)
    return primes

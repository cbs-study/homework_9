import prime_numbers as pn

input_num_check = int(input("Enter a number to check if the number is prime or not: "))
input_num_get_range = int(
    input("Enter a number to get a list of prime numbers from the range: ")
)
print(pn.check_prime_num(input_num_check))
print(pn.get_prime_num(input_num_get_range))
